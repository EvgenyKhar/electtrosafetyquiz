﻿namespace F_Delegation
{
    enum QuestionType
    {
        UnDefined,
        CaseChoosing,
        Compliance,
        Ordering,
        TextAnswering
    }
}
