﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using F_Delegation;

namespace Electrosafety
{
    class TextQuizQuestion : ElectroSafetyQuizQuestion
    {
        public TextQuizQuestion(Question fullQuestion) : base(fullQuestion)
        {

        }

        public TextQuizQuestion(ElectroSafetyQuizQuestion fullQuestion) : base(fullQuestion)
        {

        }

        public override bool GetAnswer()
        {
            //здесь выводится ответ, в котором число заменено на _
            Console.WriteLine($"\tОтвет: {rightAnswerText[0].Trim(';')}");

            float answer = -1f;
            //валидация пользовательского ввода
            while (true)
            {
                try
                {
                    Console.Write("Введите ответ: ");
                    answer = float.Parse(Console.ReadLine());
                    break;
                }
                catch
                { Console.WriteLine("Пожалуйста, введите число!"); }
            }

            return IsAnswerRight(answer);
        }

        private bool IsAnswerRight(float answer)
        {
            //проверка ответа
            if (answer == float.Parse(rightAnswerMarks[0]))
            {
                Console.WriteLine("Верно!");
                return true;
            }
            else
            {
                Console.Write("Вы ошиблись! Правильный ответ:");

                int underscoreIndex = rightAnswerText[0].IndexOf('_');
                // Заменяем _ на правильное число
                rightAnswerText[0] = "\t" + rightAnswerText[0].Remove(underscoreIndex, rightAnswerMarks[0].Length)
                                                              .Insert(underscoreIndex, rightAnswerMarks[0])
                                                              .Trim(';');

                Console.WriteLine(rightAnswerText[0]);
                return false;
            }
        }
    }
}
