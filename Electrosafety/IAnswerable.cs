﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Electrosafety
{
    interface IAnswerable
    {
        public bool GetAnswer();
    }
}
