﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace F_Delegation
{
    class Question
    {
        public List<string> questionText { get; set; }
        public QuestionType questionType { get; set; }

        public string questionForPrint { get; protected set; } //текст непосредственно вопроса

        public void PrintQuestion()
        {
            //берем первые две строки, дабы прочитать сам текст вопроса и номер вопроса
            List<string> list = this.questionText.Take(2).ToList();
            foreach (var line in list)
            {
                questionForPrint += line.Trim(';') + "\n";
            }
            Console.WriteLine(questionForPrint);
        }
    }
}
