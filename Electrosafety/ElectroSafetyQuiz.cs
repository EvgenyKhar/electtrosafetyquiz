﻿using Electrosafety;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace F_Delegation
{

    class ElectroSafetyQuiz
    {
        public List<string> AllQuestions;
        private int[] QuizTicketIndices;
        private List<ElectroSafetyQuizQuestion> TicketOfQuestions;
        private int rightAnswers = 0;

        public ElectroSafetyQuiz(string pathToQuestionzInCSV, int numOfTickets=17)
        {
            //считываем данные из файла
            try
            {
                AllQuestions = File.ReadAllLines(pathToQuestionzInCSV).ToList();
            }
            catch
            {
                Console.WriteLine("Не удалось прочитать файл. Возможно, файл открыт где-то еще.\nПопробуйте еще раз позже.");
            }
            
            QuizTicketIndices = new int[numOfTickets];
            

            FillTicketOfQuestions();

            for (int i = 0; i < TicketOfQuestions.Count; i++) 
            {
                Console.WriteLine($"Вопрос {i+1} из {TicketOfQuestions.Count}\n");
                TicketOfQuestions[i].PrintQuestion();
                if (TicketOfQuestions[i].GetAnswer()) { rightAnswers++; }

                Console.WriteLine("\nНажмите ввод для перехода к следующему вопросу\n");
                Console.WriteLine("______________\n");
                Console.ReadLine();
                Console.WriteLine("\n\n\n\n\n\n\n");
            }

            Console.WriteLine($"Билет закончен. Правильных ответов: {100 * (float)rightAnswers/TicketOfQuestions.Count}");

        }


        private void FillTicketOfQuestions()
        {
            Random r = new Random();
            for (int i = 0; i < QuizTicketIndices.Length; i++)
            {
                QuizTicketIndices[i] = r.Next(1, 306);
            }

            TicketOfQuestions = new List<ElectroSafetyQuizQuestion>();
            for (int i = 0; i < QuizTicketIndices.Length; i++)
            {
                TicketOfQuestions.Add(new ElectroSafetyQuizQuestion(GetQuestionText(i)));

                switch (TicketOfQuestions[i].questionType)
                {
                    case QuestionType.Compliance:
                        TicketOfQuestions[i] = new ComplianceQuizQuestion(TicketOfQuestions[i]);
                        break;
                    case QuestionType.CaseChoosing:
                        TicketOfQuestions[i] = new ChoosingQuizQuestion(TicketOfQuestions[i]);
                        break;
                    case QuestionType.Ordering:
                        TicketOfQuestions[i] = new OrderingQuizQuestion(TicketOfQuestions[i]);
                        break;
                    case QuestionType.TextAnswering:
                        TicketOfQuestions[i] = new TextQuizQuestion(TicketOfQuestions[i]);
                        break;
                }
            }
        }
        private Question GetQuestionText(int questionIndex)
        {
            Question currentQuestion = new Question();

            //находим строку начала и конца вопроса
            int questionStartIndex = AllQuestions.FindIndex(str => str.Trim(';') == $"Вопрос {QuizTicketIndices[questionIndex]}");
            int questionEndIndex = AllQuestions.FindIndex(
                str => str.Trim(';') == $"Вопрос {QuizTicketIndices[questionIndex] + 1}");

            currentQuestion.questionText = AllQuestions.Skip(questionStartIndex).Take(questionEndIndex - questionStartIndex).ToList();
            //Если у нас в документе есть пустые строки, то они отделяют ненужную инфу
            //берем только ту часть, которую отделили пустые строки
            if (currentQuestion.questionText.FirstOrDefault(str => str == ";") != null)
            {
                int n = currentQuestion.questionText.FindIndex(str => str == ";");
                currentQuestion.questionText = currentQuestion.questionText.Take(n).ToList();
            }
            return currentQuestion;
        }
    }
}
