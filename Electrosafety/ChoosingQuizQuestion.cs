﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using F_Delegation;

namespace Electrosafety
{
    class ChoosingQuizQuestion : ElectroSafetyQuizQuestion, IAnswerable
    {
        public ChoosingQuizQuestion(Question fullQuestion) : base(fullQuestion)
        {

        }

        public ChoosingQuizQuestion(ElectroSafetyQuizQuestion fullQuestion) : base(fullQuestion)
        {

        }

        public override bool GetAnswer()
        {
            //выводим варианты ответов с их порядковым номером
            for (int i = 0; i < rightAnswerMarks.Length; i++)
            {
                Console.WriteLine($"{i + 1}\t {rightAnswerText[i]}");
            }

            List<int> answerInt = new List<int>();
            //валидация ввода
            while (true)
            {
                try
                {
                    Console.WriteLine("Введите через запятую номера правильных ответов и нажмите ввод.");
                    List<string> answersStrings = Console.ReadLine()
                        .Trim(' ')
                        .Split(',')
                        .ToList();

                    if (answersStrings.Count <1 || answersStrings.Count > rightAnswerMarks.Length)
                    { throw new Exception(); }

                    foreach (var number in answersStrings)
                    {
                        if(int.Parse(number)<1|| int.Parse(number) > rightAnswerMarks.Length)
                        { throw new Exception(); }
                        answerInt.Add(int.Parse(number));
                    }
                    break;
                }
                catch
                {
                    Console.WriteLine("Неверный ввод");
                    Console.WriteLine($"Вводите только номера ответов." +
                        $" Номеров ответов должно быть не больше {rightAnswerMarks.Length} и не меньше 1.\n");
                }
            }
            //для проверки правильности ответа, будем в массив ответа записывать символы правильного ответа
            //после чего, почленно сравним два массива
            string[] playerAnswer = new string[rightAnswerText.Length];
            for (int i = 0; i < playerAnswer.Length; i++)
            { playerAnswer[i] = ""; }//изначально все ответы считаются не выбранными

            //а потом помечаем выбранныен ответы
            foreach (var number in answerInt)
            {
                playerAnswer[number - 1] = "(*)";
            }

            //TODO проверить правильно ли отрабатывает в данном случае функция
            return IsAnswerRight(playerAnswer);
        }
    }
}
