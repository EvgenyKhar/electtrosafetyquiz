﻿using F_Delegation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Electrosafety
{
    class OrderingQuizQuestion : ElectroSafetyQuizQuestion, IAnswerable
    {

        public OrderingQuizQuestion(Question fullQuestion) : base(fullQuestion)
        {

        }

        public OrderingQuizQuestion(ElectroSafetyQuizQuestion fullQuestion) : base(fullQuestion)
        {

        }

        public override bool GetAnswer()
        {
            //создаем массив частей соответствия, чтобы в дальнейшем можно было представить этот список в рандомном порядке
            userAnswer = new string[rightAnswerMarks.Length];
            PrintOrderingQuestion();

            List<string> playerAnswer = new List<string>();
            //валидация ввода
            while (true)
            {
                try
                {
                    Console.WriteLine("Введите через запятую номера ответов правильном порядке и нажмите ввод.");
                    List<string> answersStrings = Console.ReadLine()
                        .Trim(' ')
                        .Split(',')
                        .ToList();
                    if(answersStrings.Count!= rightAnswerMarks.Length)
                    {throw new Exception();}

                    foreach (var number in answersStrings)
                    {
                        playerAnswer.Add($"[{number}]");
                    }
                    break;
                }
                catch
                {
                    Console.WriteLine("Неверный ввод");
                    Console.WriteLine($"Вводите только номера ответов. Номеров ответов должно быть столько, сколько вариантов.\n");
                }
            }
            
            return IsAnswerRight(playerAnswer.ToArray());
        }

        private void PrintOrderingQuestion()
        {
            string[] ordering = new string[rightAnswerMarks.Length];

            for (int i = 0; i < rightAnswerText.Length; i++)
            {
                ordering[i] = rightAnswerText[i];
            }

            ShuffleArray(ordering);

            for (int i = 0; i < ordering.Length; i++)
            {
                rightAnswerMarks[rightAnswerText.ToList<string>().IndexOf(ordering[i])] = $"[{i+1}]";
                ordering[i] = $"{i+1}) {ordering[i]}";
                Console.WriteLine(ordering[i]);
            }

        }
    }
}
