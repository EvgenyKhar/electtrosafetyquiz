﻿using Electrosafety;
using System;
using System.Collections.Generic;
using System.Linq;

namespace F_Delegation
{
    class ElectroSafetyQuizQuestion: Question, IAnswerable
    {
        public bool isRightAnswer = false; //был ли дан правильный ответ
        
        public string[] rightAnswerMarks; //массив символов правильных ответов. Будет использоваться для сравнения с ответом пользователя
        public string[] rightAnswerText;//текст правильного ответа. Вместе с символами правильных ответов будет составлять полный вид правильного ответа для отображения пользователю.
        public string[] userAnswer;


        public ElectroSafetyQuizQuestion(Question fullQuestion)
        {
            questionText = fullQuestion.questionText;
            questionType = fullQuestion.questionType;

            GenerateAnswer();
            DefineQuestionType();
        }

        public ElectroSafetyQuizQuestion(ElectroSafetyQuizQuestion electroSafetyQuizQuestion)
        {
            questionText = electroSafetyQuizQuestion.questionText;
            questionType = electroSafetyQuizQuestion.questionType;
            GenerateAnswer();
        }

        private void GenerateAnswer()
        {
            //пропускаем текст вопроса и заранее подготавливаем правильный ответ
            List<string> fullAnswerText = questionText.Skip(2).ToList();

            rightAnswerText = new string[fullAnswerText.Count()];
            rightAnswerMarks = new string[rightAnswerText.Count()];

            //Если только одна строка правильного ответа, то это вопрос с текстовым ответом
            if (fullAnswerText.Count == 1)
            {
                //чтобы не заморачиваться с форматированием пользовательского ввода, ответ будем запрашивать только в виде числа.
                //поэтому символом ответа будет само число в текстовом формате, а в тексте правильного ответа число заменим на _
                foreach (char ch in fullAnswerText[0])
                {
                    if (!Char.IsDigit(ch) && ch!=',')
                    {
                        rightAnswerText[0] += ch;
                    }
                    else
                    {
                        rightAnswerMarks[0] += ch;
                        rightAnswerText[0] += '_';
                    }
                }
            }
            else
            {
                //делим варианты ответов на сами варианты и пометки правильных.
                //пометки помещаем в первый столбец, а сами вопросы помещаем во второй
                for (int i = 0; i < fullAnswerText.Count(); i++)
                {
                    rightAnswerMarks[i] = fullAnswerText[i].Split(';')[0];
                    rightAnswerText[i] = fullAnswerText[i].Split(';')[1];
                }
            }
        }

        public void DefineQuestionType()
        {
            List<string> fullAnswerText = questionText.Skip(2).ToList();
            if (fullAnswerText.Count == 1)
            {
                questionType = QuestionType.TextAnswering;
                return;
            }

            for (int i = 0; i < fullAnswerText.Count(); i++)
            {
                if (rightAnswerMarks[i] == "(*)")
                {
                    questionType = QuestionType.CaseChoosing;
                }
                else if (i != 0 && (rightAnswerMarks[i] == rightAnswerMarks[0]) && rightAnswerMarks[i] != "")
                {
                    questionType = QuestionType.Compliance;
                }   
            }

            //если мы проверили все варианты ответов, но не нашли знака (*), и не нашли дублирующегося знака правильного ответа,
            // то значит, что все символы отличаются и это вопрос на расстановку по порядку
            if (questionType==QuestionType.UnDefined)
            {
                questionType = QuestionType.Ordering;
            }
        }
        protected void ShuffleArray<T>(T[] compliancePart)
        {
            Random r = new Random();

            for (int i = 0; i < compliancePart.Length; i++)
            {
                T tmp = compliancePart[i];
                int j = r.Next(0, compliancePart.Length);
                compliancePart[i] = compliancePart[j];
                compliancePart[j] = tmp;
            }

        }
        protected void ShuffleArray<T>(T[] array, int minIndex, int maxIndex)
        {
            Random r = new Random();

            for (int i = minIndex; i < maxIndex + 1; i++)
            {
                T tmp = array[i];
                int j = r.Next(minIndex, maxIndex);
                array[i] = array[j];
                array[j] = tmp;
            }
        }
        public virtual bool GetAnswer()
        {
            throw new NotImplementedException("Base GetAnswer calling");
        }
        protected bool IsAnswerRight(string[] userAnswer)
        {
            for (int i = 0; i < rightAnswerMarks.Length; i++)
            {
                if (userAnswer[i] != rightAnswerMarks[i])
                {
                    Console.WriteLine("Вы ошиблись! Правильные ответы:");
                    for (int j = 0; j < rightAnswerMarks.Length; j++)
                    {
                        Console.WriteLine($"{rightAnswerMarks[j]} {rightAnswerText[j]}");
                    }
                    return false;
                }
            }

            //так функция выполнит эту строчку, только если массивы ответов совпадают, т.е. если все верно
            Console.WriteLine("Все верно!");
            return true;
        }
    }
}
