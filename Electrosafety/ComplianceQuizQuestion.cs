﻿using F_Delegation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Electrosafety
{
    class ComplianceQuizQuestion : ElectroSafetyQuizQuestion
    {

        public ComplianceQuizQuestion(Question fullQuestion) : base(fullQuestion)
        {

        }

        public ComplianceQuizQuestion(ElectroSafetyQuizQuestion fullQuestion) : base(fullQuestion)
        {

        }

        public override bool GetAnswer()
        {
            //создаем массив частей соответствия, чтобы в дальнейшем можно было представить этот список в рандомном порядке
            userAnswer = new string[rightAnswerMarks.Length];
            PrintComplianceQuestion();

            //запрашиваем и записываем ответ
            for (int i = 0; i < rightAnswerMarks.Length; i++)
            {
                if (i % 2 == 1)
                {
                    Console.Write($"Установите соответствие: [{(i + 1) / 2}] -> ");

                    int answer = 0;
                    try { answer = int.Parse(Console.ReadLine()); }
                    catch
                    {
                        Console.WriteLine("Некорректный ввод.\n Пожалуйста, введите число соответствующего пункта");
                        i--;
                    }
                    userAnswer[i] = "[" + answer + "]";
                }
                else
                {
                    userAnswer[i] = rightAnswerMarks[i];
                }
            }
            return IsAnswerRight(userAnswer);
        }

        private void PrintComplianceQuestion()
        {
            string[] compliance = new string[rightAnswerMarks.Length];

            //Разбиваем ответ на две части:
            //в первой половине оставляем ответы, чтобы можно было запрашивать соответствие номеру пункта, во второй убираем.
            //Первую часть соответствия помещаем в первую часть массива, чтобы вторую часть можно было отдельно перемещать.
            for (int i = 0; i < rightAnswerMarks.Length; i++)
            {
                if (i % 2 == 0)
                {
                    compliance[i / 2] = rightAnswerMarks[i] + rightAnswerText[i];
                }
                else if (i % 2 == 1)
                {
                    compliance[i / 2 + 3] =  rightAnswerText[i];
                }
            }
            ShuffleArray(compliance, minIndex: 3, maxIndex: 5);

            for (int i = 3; i < compliance.Length; i++)
            {
                rightAnswerMarks[rightAnswerText.ToList<string>().IndexOf(compliance[i])] = $"[{i - 2}]";   
                compliance[i] = (i - 2).ToString() + ")" + compliance[i];
            }

            for (int i = 0; i < compliance.Length; i++)
            {
                Console.WriteLine(compliance[i]);
            }
        }
    }
}
